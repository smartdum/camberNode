module.exports = {
    "env": {
        "node": true,
        "commonjs": true,
        "browser": true,
        "es6": true
    },
    //"extends": "plugin:vue/essential",
    "extends": ["eslint:recommended", "plugin:vue/vue3-essential"],
    "globals": {
        "process": true,
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "vue"
    ],
    "rules": {
      "no-unused-vars": "warn"
    }
};
