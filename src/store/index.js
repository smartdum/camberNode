import Vuex from 'vuex'
import axios from 'axios'
import router from '../router'
//import { mapGetters } from "vuex"

const store = new Vuex.Store({
  state: {
    mask: false,
    ajaxLoader: false,
    // données en base !!!
    infoAppli: {
      nom: 'camberAdh',
      favicon: '@/assets/logo_camberSoft.png',
      logo: '@/assets/logo_camberSoft.png',
      slogan: 'Logiciel de gestion des adhérents pour les associations',
      description: 'Logiciel de gestion des adhérents pour une association',
      mail: 'dumontmathieu@free.fr',
      site: ''
    },
    // données en base !!!
    infoAsso: {
      nom: 'Le Camber',
      logo: '@/assets/logo-camber-min.png',
      descrLogo: 'Logo Camber',
      slogan: 'Association pour la promotion de la culture traditionnelle dans le pays de Questembert',
      description: 'École de musique traditionnelle',
      mail: 'asso.contact@yahoo.fr',
      site: 'https://camber.bzh/',

    },
    entity: {
      activites: [],
      adherents: [],
      animateurs: [],
      roles: [],
      entityEc: null,
      //saisons: [],
    },
    form: {
      adherent: false,
      activite: false,
      animateur: false,
      asso: false,
    },
    formEc: {
      name: null,
      content: {},
      mode: 'add'
    },
    saisonEc: '',
    saisonFilter: 'all',
    session: {
      login: null,
      userName: null,
      token: null,
      mail: null
    },
  },
  mutations: {
    setAjaxLoader (state, bool) {
      state.ajaxLoader = bool
      // console.log(state.ajaxLoader);
    },
    setSession (state, obj) {
      //console.log(obj)
      if(obj) {
        state.session.login = obj.login
        state.session.userName = obj.prenom
        state.session.token = obj.token
        state.session.mail = obj.mail
      } else {
        state.session.login = null
        state.session.userName = null
        state.session.token = null
        state.session.mail = null
      }
      //console.log(state.session.token)
    },
    setStateForm (state, name) {
      state.mask = !state.mask
      state.form[name] = !state.form[name]
    },
    setFormEc (state, obj) {
      //console.log(obj)
      //console.log(state)
      state.formEc=obj
    },
    setSaisonEc(state) {
      const today = new Date();
      const year = today.getFullYear()
      const nextYear = today.getFullYear() + 1
      state.saisonEc = year+'-'+nextYear
      //console.log(state.saisonEc)
    },
    setSaisonFilter(state, value) {
      console.log(value)
      console.log(state.saisonFilter)
      state.saisonFilter = value
      console.log(state.saisonFilter)
    },
    //------------ SETTER dynamique pour les entités
    setEntity (state, obj) {
      //console.log(state)
      console.log(obj)
      state.entity[obj.entity]=obj.data
    },
  },
  actions: {
    //-------- GETTER dynamique
    async getEntity (context,elt) {
      //console.log(elt)
      let url = 'http://localhost:3000/get' + elt[0].toUpperCase() + elt.slice(1);
      let response = await axios.get(url,{ params: { saison: context.state.saisonEc }, headers: { 'x-access-token': context.state.session.token }})
      if (response.status == 200) {
        console.log(response)
        context.commit('setAjaxLoader', false);
        context.commit('setEntity', { data: response.data, entity: elt });
      } else {
        console.log(response.error);
        alert('Session expirée !');
        context.commit('setEntity', { data: null, entity: elt })
        context.commit('setSession', null);
        router.push('/login');
      }
    },
  },
  getters: { }
})
// console.log(store)
export default store
