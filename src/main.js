import { createApp } from 'vue'
//import { createApp, use } from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import CKEditor from '@ckeditor/ckeditor5-vue'
import App from './App.vue'
import router from './router'
import store from './store'
//import { mapGetters } from "vuex";
//import BootstrapVue from 'bootstrap-vue'
//import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

const app = createApp(App)
app.use(store)
app.use(router)
app.use(VueAxios, axios)
app.use(CKEditor)
//app.use(BootstrapVue)
//app.use(IconsPlugin)
//app.config = {}
app.config.productionTip = false;

app.mount('#app');
