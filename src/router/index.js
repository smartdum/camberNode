import { createRouter, createWebHistory } from 'vue-router'
//import VueRouter from 'vue-router'
import Accueil from '@/views/Accueil.vue'
//import NewAdhesion from '@/views/NewAdhesion.vue'
import Adherents from '@/views/Adherents.vue'
import Activites from '@/views/Activites.vue'
import Animateurs from '@/views/Animateurs.vue'
import Mail from '@/views/Mail.vue'
import Synthese from '@/views/Synthese.vue'
import Admin from '@/views/Admin.vue'
import Login from '@/views/Login.vue'
import Error from '@/views/Error.vue'

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/',
    name: 'Accueil',
    component: Accueil,
    /* redirect: {
        name: "login"
    } */
  },
  /*{
    path: '/adhesion',
    name: 'NewAdhesion',
    component: NewAdhesion
  },*/
  {
    path: '/adherents',
    name: 'Adherents',
    component: Adherents
  },
  {
    path: '/activites',
    name: 'Activites',
    component: Activites
  },
  {
    path: '/animateurs',
    name: 'Animateurs',
    component: Animateurs
  },
  {
    path: '/mail',
    name: 'Mail',
    component: Mail
  },
  {
    path: '/synthese',
    name: 'Synthese',
    component: Synthese
  },
  {
    path: '/admin',
    name: 'Admin',
    component: Admin
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'Error',
    component: Error
  },
]

const router = new createRouter({
  history: createWebHistory(process.env.BASE_URL),
  //mode: 'history',
  routes
})

export default router
