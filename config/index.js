module.exports = {
  devServer: {
    proxy: {
      '^/api': {
        target: 'http://127.0.0.1/camber/camberNode/api',
        ws: true,
        changeOrigin: true
      },
      '^/foo': {
        target: '<other_url>'
      }
    }
  }
}
