require('dotenv').config({ path: './api/middleware/.env' })
const path = require('path')
const sqlite3 = require('sqlite3').verbose()
const fs = require('fs')
const bcrypt = require('bcryptjs')

let flag = true   // temoin existence DB SQL
const dbFile = path.resolve(__dirname, './camberAdh.db')
if(!fs.existsSync(dbFile)){
  console.log("creating database file");
  fs.openSync(dbFile, "w");
  flag = false
}
console.log("BASE SQL EXISTANTE : "+flag)

const db = new sqlite3.Database(dbFile, async (err) => {
  if (err) {
    console.log(err.message)
  } else {
    console.log("Connecté à "+dbFile)

    if(flag===false) {
      //---- CREATE TABLE
      await new Promise((resolve, reject) => {
        let sql = `CREATE TABLE IF NOT EXISTS user (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          nom VARCHAR NOT NULL,
          prenom VARCHAR NOT NULL,
          mail VARCHAR NOT NULL UNIQUE,
          login VARCHAR,
          password VARCHAR,
          statut INTEGER DEFAULT 1 NOT NULL,
          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
          updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
          UNIQUE(nom,prenom,mail)
        );
        CREATE INDEX user_nom_prenom_idx ON user (nom, prenom);

        CREATE TABLE IF NOT EXISTS adherent (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          nom VARCHAR NOT NULL,
          prenom VARCHAR NOT NULL,
          mail VARCHAR NOT NULL,
          nom_tuteur VARCHAR,
          prenom_tuteur VARCHAR,
          adresse VARCHAR NOT NULL DEFAULT 'NR',
          code_postal VARCHAR NOT NULL DEFAULT 'NR',
          commune VARCHAR NOT NULL DEFAULT 'NR',
          telephone VARCHAR NOT NULL DEFAULT 'NR',
          login VARCHAR,
          password VARCHAR,
          date_adhesion DATE,
          statut INTEGER DEFAULT 1 NOT NULL,
          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
          updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
          UNIQUE(nom,prenom,mail)
        );
        CREATE INDEX adherent_nom_prenom_idx ON adherent (nom, prenom);

        CREATE TABLE IF NOT EXISTS animateur (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          nom VARCHAR NOT NULL,
          prenom VARCHAR NOT NULL,
          mail VARCHAR NOT NULL UNIQUE,
          adresse VARCHAR NOT NULL DEFAULT 'NR',
          code_postal VARCHAR NOT NULL DEFAULT 'NR',
          commune VARCHAR NOT NULL DEFAULT 'NR',
          telephone VARCHAR NOT NULL DEFAULT 'NR',
          login VARCHAR,
          password VARCHAR,
          statut INTEGER DEFAULT 1 NOT NULL,
          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
          updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
          UNIQUE(nom,prenom,mail)
        );
        CREATE INDEX animateur_nom_prenom_idx ON animateur (nom, prenom);

        CREATE TABLE IF NOT EXISTS activite (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          nom VARCHAR NOT NULL UNIQUE,
          tarif NUMERIC,
          jour VARCHAR,
          horaire VARCHAR,
          statut INTEGER DEFAULT 1 NOT NULL,
          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
          updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        );

        CREATE TABLE IF NOT EXISTS role (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          nom VARCHAR NOT NULL,
          display_order INTEGER,
          statut INTEGER DEFAULT 1 NOT NULL,
          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
          updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        );

        CREATE TABLE IF NOT EXISTS saison (
          id VARCHAR PRIMARY KEY,
          nom VARCHAR NOT NULL UNIQUE,
          statut INTEGER DEFAULT 1 NOT NULL,
          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
          updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
        );

        CREATE TABLE IF NOT EXISTS adher_acti_lnk (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          id_adherent INTEGER NOT NULL,
          id_activite INTEGER NOT NULL,
          saison VARCHAR NOT NULL,
          statut INTEGER DEFAULT 1 NOT NULL,
          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
          updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
          FOREIGN KEY (id_adherent) REFERENCES adherent(id),
          FOREIGN KEY (id_activite) REFERENCES activite(id),
          FOREIGN KEY (saison) REFERENCES saison(id),
          UNIQUE(id_adherent,id_activite)
        );

        CREATE TABLE IF NOT EXISTS anim_acti_lnk (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          id_animateur INTEGER NOT NULL,
          id_activite INTEGER NOT NULL,
          saison VARCHAR NOT NULL,
          statut INTEGER DEFAULT 1 NOT NULL,
          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
          updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
          FOREIGN KEY (id_animateur) REFERENCES animateur(id),
          FOREIGN KEY (id_activite) REFERENCES activite(id),
          FOREIGN KEY (saison) REFERENCES saison(id),
          UNIQUE(id_animateur,id_activite)
        );

        CREATE TABLE IF NOT EXISTS adher_role_lnk (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          id_adherent INTEGER NOT NULL,
          id_role INTEGER NOT NULL,
          saison VARCHAR NOT NULL,
          statut INTEGER DEFAULT 1 NOT NULL,
          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
          updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
          FOREIGN KEY (id_adherent) REFERENCES adherent(id),
          FOREIGN KEY (id_role) REFERENCES role(id),
          FOREIGN KEY (saison) REFERENCES saison(id),
          UNIQUE(id_adherent,id_role)
        );`
        db.exec(sql, function (err) {
          if (err) {
            console.log(err.message)
            reject(err)
          } else {
            console.log("CREATE TABLE EFFECTUES !")
            resolve()
          }
        })
      })

      //---- INSERTS
      await new Promise((resolve, reject) => {
        let sqlUser = `INSERT INTO user (prenom, nom, login, mail, password) VALUES (?,?,?,?,?);`
        let paramsUser = [process.env.ADMINLOGIN, process.env.ADMINLOGIN, process.env.ADMINLOGIN, process.env.ADMINEMAIL, bcrypt.hashSync(process.env.ADMINPASSWORD)]

        let sqlRole = `INSERT INTO role (nom,statut,display_order) VALUES
        ('Bénévol',1,2),
        ('Président',1,6),
        ('Secrétaire',1,5),
        ('Trésorier',1,4),
        ('Membre du CA',1,3),
        ('Ne souhaite pas participer',1,1);`

        let sqlActivite = `INSERT INTO activite (nom,statut,tarif) VALUES
        ('Accordéon',1,325),
        ('Bombarde',1,325),
        ('Cours de chant',1,90),
        ('Cours de danse',1,90),
        ('Cours d''ensemble adulte',1,75),
        ('Cours d''ensemble jeune',1,75),
        ('Éveil musical',1,80),
        ('Flûte',1,250),
        ('Guitare',1,250),
        ('Harpe',1,250),
        ('Saxophone',1,350)
        ;`

        let sqlSaison = `INSERT INTO saison (id,nom,statut) VALUES
        ('2019-2020','2019-2020',1),
        ('2020-2021','2020-2021',1),
        ('2021-2022','2021-2022',1);
        ;`

        try{
          db.serialize(function() {
            db.run(sqlUser, paramsUser)
            db.run(sqlRole)
            db.run(sqlActivite)
            db.run(sqlSaison)
          })
          console.log("INSERTS EFFECTUES !")
          resolve()
        } catch (err) {
          console.log(err.message)
          reject(err)
        }
      })

    }

  }
})

module.exports = db
