-- GET ADHERENTS
SELECT DISTINCT a.id
, a.nom
, a.prenom
, a.mail
, group_concat(a.activite
, '
, ') AS activites
, a.id_saison
, b.nom AS role
FROM (
	SELECT a.id
	, a.nom
	, a.prenom
	, a.mail
	, a.id_role
	, CASE WHEN b.id_activite IS NULL THEN 0 ELSE b.id_activite END AS id_activite
	, CASE WHEN c.nom IS NULL THEN 'NR' ELSE c.nom END AS activite
	, c.nom
	, a.id_saison FROM adherent AS a
	LEFT JOIN adher_acti_lnk AS b ON a.id=b.id_adherent
	LEFT JOIN activite AS c ON b.id_activite=c.id
	WHERE
	(a.statut = 1 AND b.statut = 1 AND c.statut = 1)
	AND
	(a.id_saison = 1 AND b.id_saison = 1)
) AS a
LEFT JOIN role AS b ON a.id_role=b.id
LEFT JOIN saison AS c ON a.id_saison=c.id
WHERE b.statut = 1 AND a.id_role <> 2
GROUP BY a.id
, a.nom
, a.prenom
, a.mail
, role;


-- GET ACTIVITES
SELECT a.id
, a.nom
, a.tarif
, b.id_animateur
, c.nom AS animateur
FROM activite AS a
LEFT JOIN anim_acti_lnk AS b on a.id = b.id_activite
LEFT JOIN animateur AS c ON b.id_animateur = c.id
WHERE a.statut = 1 AND b.statut = 1 AND c.statut = 1
AND b.id_saison = 1
ORDER BY a.nom ASC;
