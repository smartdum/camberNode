// node ./api/nodeJs/apiCamberAdh.js
const express = require("express");
const cors = require('cors')
const sqlite3 = require("sqlite3").verbose();
const bodyParser = require('body-parser');

// Création du serveur Express
const app = express();

// Server configuration
app.use(cors());
app.options('*', cors());
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));

// open database in memory
const dbFile = './api/camberAdh.db'
const db = new sqlite3.Database(dbFile, sqlite3.OPEN_READWRITE, (err) => {
  if (err) {
    console.error(err.message);
  }
  console.log('Connecté à camberAdh database.');
});

// Démarrage du serveur
app.listen(3000, () => {
  console.log("Serveur démarré (http://localhost:3000/) !");
});

// GET connexion
app.get("/getConnexion", (req, res) => {
  //console.log(req.query)
  //console.log(req.query.password)
  const sql = 'SELECT count(a.id) AS nb FROM adherent AS a JOIN adher_role_lnk AS b ON a.id = b.id_adherent WHERE a.login = ? AND a.password = ? AND b.saison = ?';
  const params = [req.query.login, req.query.password, req.query.saison];
  //console.log(params)
  //console.log(sql)
  db.all(sql, params, (err, result) => {
    if (err) {
      return console.error(err.message);
    }
    //console.log(result);
    res.send(result);
  });
});

// GET roles
app.get("/getRoles", (req, res) => {
  //const sql = "SELECT * FROM role";
  const sql = "SELECT id, nom FROM role WHERE statut = 1 ORDER BY display_order";
  db.all(sql, [], (err, result) => {
    if (err) {
      return console.error(err.message);
    }
    //console.log(result);
    res.send(result);
  });
});

// GET adherents
app.get("/getAdherents", (req, res) => {
  //const param = req.query.state;
  //console.log(req)
/*  var filter
  if(param=="true") {
    filter = [" a.statut = 1 "]
  } else {
    filter = [" a.statut = 1 AND a.id_saison = ? AND b.id_saison = ? "]
  } */
  //console.log(filter)
  const sql = "SELECT DISTINCT a.id, a.nom, a.prenom, a.mail , group_concat(a.activite, ', ') AS activites , a.saison, b.nom AS role FROM ( SELECT a.id, a.nom, a.prenom, a.mail , CASE WHEN b.id_activite IS NULL THEN 0 ELSE b.id_activite END AS id_activite , CASE WHEN c.nom IS NULL THEN 'NR' ELSE c.nom END AS activite , b.saison FROM adherent AS a LEFT JOIN adher_acti_lnk AS b ON a.id=b.id_adherent LEFT JOIN activite AS c ON b.id_activite=c.id WHERE (a.statut = 1 AND b.statut = 1 AND c.statut = 1) AND b.saison = ? ) AS a LEFT JOIN ( SELECT a.id_adherent, b.nom FROM adher_role_lnk AS a JOIN role AS b ON a.id_role = b.id WHERE ( a.statut = 1 AND b.statut = 1 ) AND a.saison = ? ) AS b ON a.id=b.id_adherent GROUP BY a.id, a.nom, a.prenom, a.mail, b.nom;";
  //console.log(sql)
  //console.log(req.query.saison)
  const params = [req.query.saison];
  db.all(sql, params, (err, result) => {
    if (err) {
      return console.error(err.message);
    }
    //console.log(result);
    res.send(result);
  });
});


//------------------------------------------------
//---- REQUETE ajout nouvel adhérent (syntaxe pas terrible => pas de promise !!!)
//https://www.npmjs.com/package/sqlite-asyn
//https://dev.to/lampewebdev/getting-sqlite-running-with-await-async-and-expressjs-polkajs-2b0g
app.get("/addNewAdherent", async (req, res) => {
  //console.log(req.query)
  var retourAjax = {}
  retourAjax.state=0
  retourAjax.rowId=null
  retourAjax.msg="Enregistrement pas encore effectué !"

  //---- VERIF PERSONNE ENREGISTREE
  let sqlVerif = "SELECT count(*) AS nb FROM adherent WHERE UPPER(nom) LIKE UPPER( ? ) AND UPPER(prenom) LIKE UPPER( ? ) AND mail LIKE ? AND id_saison = ?"
  let paramsVerif = [req.query.nom, req.query.prenom, req.query.mail, req.query.saison]
  //console.log(paramsVerif)
  db.get(sqlVerif, paramsVerif, (err, result) => {
    if (err) {
      console.log(err.message);
      retourAjax.state=0
      retourAjax.msg=err.message
      res.send(retourAjax)
      return
    }
    console.log(result)

    if(result.nb == 0) {
      //---- ENRISTREMENT ADHERENT
      try {
        let sqlInsertPers = "INSERT INTO adherent (nom, prenom, mail, nom_tuteur, prenom_tuteur, adresse, code_postal, commune, id_role, id_saison) VALUES ( TRIM(UPPER( ? )), TRIM( ? ), TRIM( ? ), TRIM(UPPER( ? )), TRIM( ? ), TRIM( ? ), TRIM( ? ), TRIM(UPPER( ? )), TRIM( ? ), TRIM( ? ) )"
        let paramsInsertPers = [req.query.nom, req.query.prenom, req.query.mail, req.query.nomTuteur, req.query.prenomTuteur, req.query.adresse, req.query.codePostal, req.query.commune, req.query.role, req.query.saison ]
        db.run(sqlInsertPers, paramsInsertPers, function(err) {
          if (err) {
            console.log(err.message);
            retourAjax.state=0
            retourAjax.msg=err.message
            res.send(retourAjax)
            return
          } else {
            console.log('INSERT adherent OK')
            console.log(`lastID : ${this.lastID}`);
            retourAjax.rowId=this.lastID

            //---- ENRISTREMENT ACTIVITE
            let sqlInsertActi = "INSERT INTO adher_acti_lnk (id_adherent, id_activite, id_saison) VALUES ( TRIM( ? ), TRIM( ? ), TRIM( ? ) )"
            var stmt = db.prepare(sqlInsertActi);
            for (var i = 0; i < req.query.activites.length; i++) {
              stmt.run(this.lastID, req.query.activites[i], req.query.saison);
            }
            stmt.finalize()
            //var test=stmt.finalize()
            //console.log(test)

            console.log('INSERT activite OK')
            retourAjax.state=1
            retourAjax.msg="Enregistrement effectué ! Personne inscrite !"
            res.send(retourAjax)
            return
          }
        });
      } finally {
        console.log('OK')
      }
    } else {
      retourAjax.state=1
      retourAjax.msg="Enregistrement non effectué ! Personne déjà inscrite !"
      res.send(retourAjax)
      return
    }
  });
});

//------------------------------------------------
//---- REQUETE liste des activités
app.get("/getActivites", (req, res) => {
  //console.log(req)
  //console.log(res)
  const sql = "SELECT a.id, a.nom, a.tarif , b.nom AS animateur FROM activite AS a LEFT JOIN ( SELECT a.id_activite, b.prenom||' '||b.nom AS nom FROM anim_acti_lnk AS a LEFT JOIN animateur AS b ON a.id_animateur = b.id WHERE a.statut = 1 AND b.statut = 1 AND a.saison = ? ) AS b ON a.id = b.id_activite WHERE a.statut = 1 ORDER BY a.nom ASC;";
  //console.log(sql)
  const params = [req.query.saison];
  db.all(sql, params, (err, result) => {
    if (err) {
      return console.error(err.message);
    }
    //console.log(result);
    res.send(result);
  });
});

//------------------------------------------------
//---- REQUETE création activité
app.get("/addNewActivite", (req, res) => {
  //console.log(req.query)
  var sqlVerif = "SELECT count(*) AS nb FROM activite WHERE UPPER(nom) LIKE UPPER( ? ) AND id_animateur = ? "
  var paramsVerif = [req.query.nom, req.query.responsable]
  var flag
  //console.log(paramsVerif)
  db.get(sqlVerif, paramsVerif, (errVerif, resultVerif) => {
    if (errVerif) {
      return console.error(errVerif.message)
    }
    //console.log(resultVerif)
    //res.send(resultVerif);
    flag = resultVerif
    //console.log(flag)

    const retourAjax = {}
    if(flag.nb == 0) {
      var sqlInsert = "INSERT INTO activite (nom, id_animateur, tarif, statut) VALUES ( TRIM(UPPER( ? )), TRIM( ? ), TRIM( ? ), 1 )"
      var paramsInsert = [req.query.nom, req.query.responsable, req.query.tarif ]
      // insert one row into the langs table
      db.run(sqlInsert, paramsInsert, function(err) {
        if (err) {
          return console.log(err.message);
        }
        // get the last insert id
        //console.log(`A row has been inserted with rowid ${this.lastID}`);
        retourAjax.state=1
        retourAjax.msg="Enregistrement effectué ! Activité enregistrée !"
        retourAjax.rowId=this.lastID
        res.send(retourAjax)
      });
    } else {
      retourAjax.state=0
      retourAjax.msg="Enregistrement non effectué ! Activité déjà enregistrée !"
      retourAjax.rowId=null
      res.send(retourAjax)
    }
  });
});

//------------------------------------------------
//---- REQUETE liste animateurs
app.get("/getAnimateurs", (req, res) => {
  const sql = "SELECT DISTINCT a.id_animateur AS id, b.nom, b.prenom, b.mail, b.adresse, b.code_postal AS codePostal, b.commune, b.telephone, group_concat(c.nom, ', ') AS activites FROM anim_acti_lnk AS a LEFT JOIN animateur AS b ON a.id_animateur = b.id JOIN activite AS c ON a.id_activite = c.id WHERE a.statut = 1 AND b.statut = 1 AND a.saison = ? ORDER BY b.nom ASC;";
  const params = [req.query.saison];
  db.all(sql, params, (err, result) => {
    if (err) {
      return console.error(err.message);
    }
    //console.log(result);
    res.send(result);
  });
});

//------------------------------------------------
//---- REQUETE création animateurs
app.get("/addNewAnimateur", (req, res) => {
  //console.log(req.query)
  var sqlVerif = "SELECT count(*) AS nb FROM animateur WHERE UPPER(nom) LIKE UPPER( ? ) AND UPPER(prenom) LIKE UPPER( ? ) AND mail LIKE ? "
  var paramsVerif = [req.query.nom, req.query.prenom, req.query.mail]
  var flag
  //console.log(paramsVerif)
  db.get(sqlVerif, paramsVerif, (errVerif, resultVerif) => {
    if (errVerif) {
      return console.error(errVerif.message)
    }
    //console.log(resultVerif)
    //res.send(resultVerif);
    flag = resultVerif
    //console.log(flag)

    const retourAjax = {}
    if(flag.nb == 0) {
      var sqlInsert = "INSERT INTO animateur (nom, prenom, mail, adresse, code_postal, commune, telephone) VALUES ( TRIM(UPPER( ? )), TRIM( ? ), TRIM( ? ), TRIM( ? ), TRIM( ? ), TRIM(UPPER( ? )), TRIM( ? ) )"
      var paramsInsert = [req.query.nom, req.query.prenom, req.query.mail, req.query.adresse, req.query.codePostal, req.query.commune, req.query.telephone ]
      // insert one row into the langs table
      db.run(sqlInsert, paramsInsert, function(err) {
        if (err) {
          return console.log(err.message);
        }
        // get the last insert id
        //console.log(`A row has been inserted with rowid ${this.lastID}`);
        retourAjax.state=1
        retourAjax.msg="Enregistrement effectué ! Animateur inscrit !"
        retourAjax.rowId=this.lastID
        res.send(retourAjax)
      });
    } else {
      retourAjax.state=0
      retourAjax.msg="Enregistrement non effectué ! Animateur déjà inscrit !"
      retourAjax.rowId=null
      res.send(retourAjax)
    }
  });
});
