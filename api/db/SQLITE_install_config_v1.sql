/*
sudo apt update
sudo apt install sqlite3
sudo add-apt-repository -y ppa:linuxgndu/sqlitebrowser
sudo apt update
sudo apt install sqlitebrowser
*/
-- https://fr.jeffprod.com/blog/2015/chiffrer-une-base-sqlite/

PRAGMA foreign_keys=off;
--BEGIN TRANSACTION;

/* DROP TABLE IF EXISTS statut;
CREATE TABLE statut (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nom VARCHAR NOT NULL
); */

DROP TABLE IF EXISTS personne;
CREATE TABLE personne (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nom VARCHAR NOT NULL,
  prenom VARCHAR NOT NULL,
  mail VARCHAR NOT NULL,
  nom_tuteur VARCHAR,
  prenom_tuteur VARCHAR,
  adresse VARCHAR NOT NULL DEFAULT 'NR',
  code_postal VARCHAR NOT NULL DEFAULT 'NR',
  commune VARCHAR NOT NULL DEFAULT 'NR',
  telephone VARCHAR NOT NULL DEFAULT 'NR',
  activite_id VARCHAR[],
  role_id INTEGER,
  statut_id INTEGER DEFAULT 1,
  login VARCHAR,
  password VARCHAR,
  date_adhesion DATE,
  FOREIGN KEY (role_id) REFERENCES role(id),
  -- FOREIGN KEY (statut_id) REFERENCES statut(id),
  FOREIGN KEY (activite_id) REFERENCES activite(id)
);
CREATE INDEX "personne_nom_prenom_idx" ON "personne" (nom, prenom);

DROP TABLE IF EXISTS activite;
CREATE TABLE activite (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nom VARCHAR NOT NULL,
  personne_id INTEGER NOT NULL,
  tarif NUMERIC,
  jour VARCHAR,
  horaire VARCHAR,
  statut_id INTEGER DEFAULT 1,
  FOREIGN KEY (personne_id) REFERENCES personne(id)
  -- ,FOREIGN KEY (statut_id) REFERENCES statut(id)
);

DROP TABLE IF EXISTS role;
CREATE TABLE role (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nom VARCHAR NOT NULL,
  display_order INTEGER,
  statut_id INTEGER DEFAULT 1
  -- ,FOREIGN KEY (statut_id) REFERENCES statut(id)
);

DROP TABLE IF EXISTS pers_acti_lnk;
CREATE TABLE pers_acti_lnk (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  id_personne INTEGER NOT NULL,
  id_activite INTEGER NOT NULL,
  statut_id INTEGER DEFAULT 1,
  saison VARCHAR NOT NULL
  -- ,FOREIGN KEY (statut_id) REFERENCES statut(id)
);

/* INSERT INTO statut (nom) VALUES
('actif'),
('inactif'); */

INSERT INTO personne (nom ,prenom, mail) VALUES
('test','test','test');

INSERT INTO role (nom,statut_id,display_order) VALUES
('Bénévol',1,2),
('Professeur',1,7),
('Président',1,6),
('Secrétaire',1,5),
('Trésorier',1,4),
('Membre du CA',1,3),
('Ne souhaite pas participer',1,1);

INSERT INTO activite (nom,statut_id,personne_id,tarif) VALUES
('Accordéon',1,1,325),
('Bombarde',1,1,325),
('Cours de chant',1,1,90),
('Cours de danse',1,1,90),
('Cours d''ensemble adulte',1,1,75),
('Cours d''ensemble jeune',1,1,75),
('Éveil musical',1,1,80),
('Flûte',1,1,250),
('Guitare',1,1,250),
('Harpe',1,1,250),
('Saxophone',1,1,350)
;

--COMMIT;
PRAGMA foreign_keys=on;

INSERT INTO personne (nom, prenom, mail, login, password, role_id)
VALUES
('DUMONT', 'mathieu', 'dumontmathieu@free.fr', 'matdum', 'md35600', 6);

-- UPDATE personne SET login = 'matdum', password = 'md35600' WHERE id = 2;

UPDATE personne SET adresse = 'NR' WHERE adresse IS NULL OR LENGTH(adresse) = 0;
UPDATE personne SET code_postal = 'NR' WHERE LENGTH(code_postal) = 0;
UPDATE personne SET commune = 'NR' WHERE commune IS NULL OR LENGTH(commune) = 0;
UPDATE personne SET telephone = 'NR' WHERE telephone IS NULL OR LENGTH(telephone) = 0;

ALTER TABLE personne SET adresse NOT NULL DEFAULT 'NR';

INSERT INTO pers_acti_lnk (id_personne,id_activite) VALUES
(2,2),
(2,3),
(2,4);

ALTER TABLE pers_acti_lnk ADD COLUMN saison VARCHAR DEFAULT '2020-2021' NOT NULL;

PRAGMA foreign_keys=off;
DROP TABLE IF EXISTS statut;
UPDATE personne SET statut_id = 0 WHERE statut_id = 2;
UPDATE activite SET statut_id = 0 WHERE statut_id = 2;
UPDATE role SET statut_id = 0 WHERE statut_id = 2;
UPDATE pers_acti_lnk SET statut_id = 0 WHERE statut_id = 2;

PRAGMA foreign_keys=off;
ALTER TABLE personne DROP CONSTRAINT FOREIGN KEY("statut_id") REFERENCES "statut"("id")
