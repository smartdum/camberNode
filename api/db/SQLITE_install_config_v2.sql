/*
sudo apt update
sudo apt install sqlite3
sudo add-apt-repository -y ppa:linuxgndu/sqlitebrowser
sudo apt update
sudo apt install sqlitebrowser
*/
-- https://fr.jeffprod.com/blog/2015/chiffrer-une-base-sqlite/
 
PRAGMA foreign_keys=off;
--BEGIN TRANSACTION;

CREATE TABLE IF NOT EXISTS user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nom VARCHAR NOT NULL,
  prenom VARCHAR NOT NULL,
  mail VARCHAR NOT NULL UNIQUE,
  login VARCHAR,
  password VARCHAR,
  statut INTEGER DEFAULT 1 NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  UNIQUE(nom,prenom,mail)
);
CREATE INDEX user_nom_prenom_idx ON user (nom, prenom);

CREATE TABLE IF NOT EXISTS adherent (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nom VARCHAR NOT NULL,
  prenom VARCHAR NOT NULL,
  mail VARCHAR NOT NULL,
  nom_tuteur VARCHAR,
  prenom_tuteur VARCHAR,
  adresse VARCHAR NOT NULL DEFAULT 'NR',
  code_postal VARCHAR NOT NULL DEFAULT 'NR',
  commune VARCHAR NOT NULL DEFAULT 'NR',
  telephone VARCHAR NOT NULL DEFAULT 'NR',
  login VARCHAR,
  password VARCHAR,
  date_adhesion DATE,
  statut INTEGER DEFAULT 1 NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  UNIQUE(nom,prenom,mail)
);
CREATE INDEX adherent_nom_prenom_idx ON adherent (nom, prenom);

CREATE TABLE IF NOT EXISTS animateur (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nom VARCHAR NOT NULL,
  prenom VARCHAR NOT NULL,
  mail VARCHAR NOT NULL UNIQUE,
  adresse VARCHAR NOT NULL DEFAULT 'NR',
  code_postal VARCHAR NOT NULL DEFAULT 'NR',
  commune VARCHAR NOT NULL DEFAULT 'NR',
  telephone VARCHAR NOT NULL DEFAULT 'NR',
  login VARCHAR,
  password VARCHAR,
  statut INTEGER DEFAULT 1 NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  UNIQUE(nom,prenom,mail)
);
CREATE INDEX animateur_nom_prenom_idx ON animateur (nom, prenom);

CREATE TABLE IF NOT EXISTS activite (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nom VARCHAR NOT NULL UNIQUE,
  tarif NUMERIC,
  jour VARCHAR,
  horaire VARCHAR,
  statut INTEGER DEFAULT 1 NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE IF NOT EXISTS role (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nom VARCHAR NOT NULL,
  display_order INTEGER,
  statut INTEGER DEFAULT 1 NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE IF NOT EXISTS saison (
  id VARCHAR PRIMARY KEY,
  nom VARCHAR NOT NULL UNIQUE,
  statut INTEGER DEFAULT 1 NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE IF NOT EXISTS adher_acti_lnk (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  id_adherent INTEGER NOT NULL,
  id_activite INTEGER NOT NULL,
  saison VARCHAR NOT NULL,
  statut INTEGER DEFAULT 1 NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  FOREIGN KEY (id_adherent) REFERENCES adherent(id),
  FOREIGN KEY (id_activite) REFERENCES activite(id),
  FOREIGN KEY (saison) REFERENCES saison(id),
  UNIQUE(id_adherent,id_activite)
);

CREATE TABLE IF NOT EXISTS anim_acti_lnk (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  id_animateur INTEGER NOT NULL,
  id_activite INTEGER NOT NULL,
  saison VARCHAR NOT NULL,
  statut INTEGER DEFAULT 1 NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  FOREIGN KEY (id_animateur) REFERENCES animateur(id),
  FOREIGN KEY (id_activite) REFERENCES activite(id),
  FOREIGN KEY (saison) REFERENCES saison(id),
  UNIQUE(id_animateur,id_activite)
);

CREATE TABLE IF NOT EXISTS adher_role_lnk (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  id_adherent INTEGER NOT NULL,
  id_role INTEGER NOT NULL,
  saison VARCHAR NOT NULL,
  statut INTEGER DEFAULT 1 NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  FOREIGN KEY (id_adherent) REFERENCES adherent(id),
  FOREIGN KEY (id_role) REFERENCES role(id),
  FOREIGN KEY (saison) REFERENCES saison(id),
  UNIQUE(id_adherent,id_role)
);

INSERT INTO saison (id,nom,statut) VALUES
('2019-2020','2019-2020',1),
('2020-2021','2020-2021',1),
('2021-2022','2021-2022',1);

INSERT INTO adherent (nom ,prenom, mail) VALUES
('test','test','test');

INSERT INTO role (nom,statut,display_order) VALUES
('Bénévol',1,2),
('Président',1,6),
('Secrétaire',1,5),
('Trésorier',1,4),
('Membre du CA',1,3),
('Ne souhaite pas participer',1,1);

INSERT INTO activite (nom,statut,tarif) VALUES
('Accordéon',1,325),
('Bombarde',1,325),
('Cours de chant',1,90),
('Cours de danse',1,90),
('Cours d''ensemble adulte',1,75),
('Cours d''ensemble jeune',1,75),
('Éveil musical',1,80),
('Flûte',1,250),
('Guitare',1,250),
('Harpe',1,250),
('Saxophone',1,350)
;

INSERT INTO adherent (nom, prenom, mail, login, password)
VALUES
('DUMONT', 'mathieu', 'dumontmathieu@free.fr', 'matdum', 'camber'),
('SARAH', 'sarah', 'sarahsarah@free.fr', NULL, NULL),
('DUMONT', 'youenn', 'dumontmathieu@free.fr', NULL, NULL);

INSERT INTO adher_role_lnk (id_adherent,id_role,saison) VALUES
(2,5,'2021-2022');

INSERT INTO adher_acti_lnk (id_adherent,id_activite,saison) VALUES
(2,2,'2021-2022'),
(2,3,'2021-2022'),
(2,4,'2021-2022'),
(3,2,'2021-2022'),
(3,3,'2021-2022'),
(1,4,'2021-2022');

INSERT INTO anim_acti_lnk (id_animateur,id_activite,saison) VALUES
(2,2,'2021-2022'),
(1,3,'2021-2022'),
(2,4,'2021-2022');

INSERT INTO animateur (nom, prenom, mail)
VALUES
('POTTIER', 'Pascal', 'pascal.pottier@yahoo.fr'),
('DREANO', 'Goulven', 'goulven.dreano@yahoo.fr');

--COMMIT;
PRAGMA foreign_keys=on;
