const jwt = require('jsonwebtoken')

const verifyToken = (req, res, next) => {
  if(req.headers) {
    const token = req.headers['x-access-token']
    //console.log(token)
    jwt.verify(token, process.env.TOKEN_KEY, (err, user) => {
      if (err) {
        console.log('token erronné !')
        return res.sendStatus(403)
      } else {
        //console.log('token en cours !')
        req.user = user
        next()
      }
    })
  } else {
    //console.log('token nul !')
    return res.sendStatus(401)
  }
}

module.exports = verifyToken
