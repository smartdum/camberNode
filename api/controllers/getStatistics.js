const verifyToken = require('../middleware/verifyToken.js')

module.exports = (app, db) => {
  //---- SELECT
  app.get("/getStatistics", verifyToken, (req, res) => {
    //console.log(req)
    if (req.user) {
      let sql = `SELECT DISTINCT b.id, b.nom, count(*) AS nb
      FROM adher_acti_lnk AS a
      LEFT JOIN activite AS b ON a.id_activite = b.id
      WHERE a.statut= 1 AND b.statut = 1
      AND a.saison = ?
      GROUP BY b.id, b.nom
      ORDER BY nb DESC, nom;`
      //console.log(sql)
      let params = [req.query.saison]
      db.all(sql, params, (err, rows) => {
        if (err) {
          return console.error(err.message)
        }
        res.send(rows)
      })
    } else {
      console.log('token expiré !')
      res.status(403).send({"msg": "Session expirée !"})
    }
  })

}
