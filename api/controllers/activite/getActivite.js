const verifyToken = require('../../middleware/verifyToken.js')

module.exports = (app, db) => {
  //---- SELECT
  app.get("/getActivites", verifyToken, (req, res) => {
    //http://localhost:3000/activites?saison=2021-2022
    //console.log(req)
    if (req.user) {
      let sql = `SELECT a.id, a.nom, a.tarif
      , CASE
        WHEN foo.nom IS NOT NULL THEN group_concat(foo.nom, ', ')
        ELSE 'NR'
      END AS animateur
      FROM activite AS a
      LEFT JOIN (
        SELECT a.id_activite
        , b.prenom||' '||b.nom AS nom
        FROM anim_acti_lnk AS a
        LEFT JOIN animateur AS b ON a.id_animateur = b.id
        WHERE a.statut = 1 AND b.statut = 1 AND a.saison = ?
      ) AS foo ON a.id = foo.id_activite
      WHERE a.statut = 1
      GROUP BY a.id, a.nom, a.tarif
      ORDER BY a.nom ASC;`
      //console.log(sql)
      let params = [req.query.saison]
      db.all(sql, params, (err, rows) => {
        if (err) {
          return console.error(err.message)
        }
        res.send(rows)
      })
    } else {
      console.log('token expiré !')
      res.status(403).send({"msg": "Session expirée !"})
    }
  })

}
