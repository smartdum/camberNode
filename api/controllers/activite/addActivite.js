const verifyToken = require('../../middleware/verifyToken.js')

module.exports = (app, db) => {
  app.post("/addActivite", verifyToken, async (req, res) => {
    //console.log(req.body)
    if (req.user) {
      let retourAjax = {}

      //--- VERIF EXISTENCE ACTIVITE
      let verif = await new Promise((resolve, reject) => {
        let sql = `SELECT count(a.id_activite) AS nb
        FROM anim_acti_lnk AS a
        JOIN activite AS b ON a.id_activite = b.id
        JOIN animateur AS c ON a.id_animateur = c.id
        WHERE TRIM(LOWER(b.nom)) LIKE TRIM(LOWER( ? ))
        AND (TRIM(LOWER(c.nom))||' '||TRIM(LOWER(c.prenom))) = TRIM(LOWER( ? ));`
        let params = [req.body.nom, req.body.animateur]
        let stmt = db.prepare(sql);
        stmt.get(params , function(err, row) {
          if (err) {
            console.log(err)
            reject(err)
          } else {
            resolve(row)
          }
        })
        stmt.finalize()
      })
      //console.log(verif)

      if(verif.nb == 0) {
        try {
          //--- ENREGISTREMENT ACTIVITE
          let insert1 = await new Promise((resolve, reject) => {
            let sql = "INSERT INTO activite (nom, tarif, created_at) VALUES ( TRIM( ? ), TRIM( ? ), ? )"
            let params = [req.body.nom, req.body.tarif, Date('now')]
            let stmt = db.prepare(sql);
            stmt.run(params, function(err) {
              if (err) {
                console.log(err)
                reject(err)
              } else {
                resolve(stmt)
              }
            })
            stmt.finalize()
          })
          //console.log(`"ID activité : ${insert1.lastID}`)

          //--- ENREGISTREMENT LIAISON ACTIVITE / ANIMATEUR
          await new Promise((resolve, reject) => {
            let sql = "INSERT INTO anim_acti_lnk (id_animateur, id_activite, saison, created_at) VALUES ( ? , ? , TRIM( ? ), ? )"
            let params = [req.body.animateur, insert1.lastID, req.body.saison, Date('now')]
            let stmt = db.prepare(sql)
            stmt.run(params, function(err) {
              if (err) {
                console.log(err)
                reject(err)
              } else {
                resolve(stmt)
              }
            })
            stmt.finalize()
          })

          retourAjax.state=1
          retourAjax.msg="Enregistrement effectué ! Activité inscrite !"
          retourAjax.rowId=insert1.lastID
        } catch (e) {
          console.log(e)
          retourAjax.state=0
          retourAjax.msg="Enregistrement NON effectué ! Activité NON inscrite !"
          retourAjax.rowId=null
        }
      } else {
        retourAjax.state=0
        retourAjax.msg="Enregistrement non effectué ! Activité déjà enregistrée !!!"
        retourAjax.rowId=null
      }
      console.log(retourAjax.msg)
      res.send(retourAjax)
    } else {
      console.log('token expiré !')
      res.status(403).send({"msg": "Session expirée !"})
    }
  })

}
