const verifyToken = require('../../middleware/verifyToken.js')

module.exports = (app, db) => {
  app.patch("/editActivite/:id", verifyToken, async (req, res) => {
    //console.log(req.body)
    let retourAjax = {}
    if (req.user) {
      try {
        //---- ACTIVITE
        let update1 = await new Promise((resolve, reject) => {
          let sql = `UPDATE activite SET (nom, tarif, updated_at) = ( TRIM( ? ), ? , ? ) WHERE id = ? ;`
          console.log(sql)
          let params = [req.body.nom, req.body.tarif, Date('now'), req.body.id]
          console.log(params)
          let stmt = db.prepare(sql)
          stmt.run(params, function(err) {
            if (err) {
              console.log(err)
              reject(err)
            } else {
              resolve(stmt)
            }
          })
          stmt.finalize()
        })
        console.log(update1)

        //---- VERIF existence LIAISON ACTIVITE / ANIMATEUR
        let verif = await new Promise((resolve, reject) => {
          let sql = "SELECT count(*) AS nb FROM anim_acti_lnk WHERE id_activite = ? AND id_animateur = ? AND saison = ? ;"
          let params = [req.body.id, req.body.animateur, req.body.saison]
          let stmt = db.prepare(sql);
          stmt.get(params , function(err, rows) {
            if (err) {
              console.log(err)
              reject(err)
            } else {
              resolve(rows)
            }
          })
          stmt.finalize()
        })
        console.log(verif)

        //---- MAJ LIAISON ACTIVITE / ANIMATEUR
        var sql, params
        if(verif.nb==0) {
          // SI PAS liaison => INSERT
          sql = "INSERT INTO anim_acti_lnk (id_animateur, id_activite, saison, created_at) VALUES ( ? , ? , TRIM( ? ), ? )"
          params = [req.body.animateur, req.body.id, req.body.saison, Date('now')]
        } else {
          // SI liaison => UPDATE
          sql = `UPDATE anim_acti_lnk SET (id_animateur, saison, updated_at) = ( ? , TRIM( ? ), ? ) WHERE id_activite = ? AND saison = ? ;`
          params = [req.body.animateur, req.body.saison, Date('now'), req.body.id, req.body.saison]
        }
        //console.log(sql)
        //console.log(params)
        await new Promise((resolve, reject) => {
          let stmt = db.prepare(sql)
          stmt.run(params, function(err) {
            if (err) {
              console.log(err)
              reject(err)
            } else {
              resolve()
            }
          })
          stmt.finalize()
        })

        retourAjax.state=1
        retourAjax.msg="Enregistrement effectué ! Activité mise à jour !"
        retourAjax.rowId=req.body.id
      } catch (e) {
        console.log(e)
        retourAjax.state=0
        retourAjax.msg="Enregistrement NON effectué ! Activité NON mise à jour !"
        retourAjax.rowId=null
      }
      //console.log(retourAjax)
      res.send(retourAjax)
    } else {
      console.log('token expiré !')
      res.status(403).send({"msg": "Session expirée !"})
    }
  })

}
