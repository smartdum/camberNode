const verifyToken = require('../middleware/verifyToken.js')

module.exports = (app, db) => {
  app.get("/getSaisons", verifyToken, (req, res) => {
    if (req.user) {
      let sql = "SELECT id, nom FROM saison WHERE statut = 1 ORDER BY id DESC"
      db.all(sql, [], (err, result) => {
        if (err) {
          return console.error(err.message)
        }
        //console.log(result)
        res.send(result)
      })
    } else {
      console.log('token expiré !')
      return res.sendStatus(403)
    }
  })

}
