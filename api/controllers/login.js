//require("dotenv").config()
require('dotenv').config({ path: './api/middleware/.env' })
const jwt = require("jsonwebtoken")
const bcrypt = require('bcryptjs')

module.exports = (app, db) => {
  app.post("/login", async (req, res) => {
    //console.log(process.env.TOKEN_KEY)
    try {
      //console.log(req.body)
      const { login, password, saison } = req.body
      if (!(login && password && saison)) {
        res.status(400).send("Infos manquantes pour authentification !")
      }
      let user = []

      // VERIF USER BDD
      let params = [req.body.login]
      //let sql = 'SELECT a.id, a.prenom, a.nom, a.login, a.mail, a.password FROM adherent AS a JOIN adher_role_lnk AS b ON a.id = b.id_adherent WHERE a.login = ? AND b.saison = ? '
      let sql = 'SELECT id, prenom, nom, login, mail, password FROM user WHERE login = ? AND statut = 1'
      //console.log(sql)
      db.all(sql, params, (err, rows) => {
        //console.log(rows)
        if (err){
          res.status(400).json({"error": err.message})
          return
        } else {
          user = rows[0]
        }
        //console.log(user)

        //---- VERIF PASSWORD BDD
        let passwordIsValid = bcrypt.compareSync(password, user.password)
        //console.log(passwordIsValid)
        if(passwordIsValid === true) {
          //console.log('Utilisateur authentifié !')
          const token = jwt.sign({login}, process.env.TOKEN_KEY, { expiresIn: "30m" })
          user.token = token
        } else {
          //console.log('Utilisateur NON authentifié !')
          return res.status(400).send(user)
        }
        //console.log(user)
        let d = new Date()
        let hours = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds()
        console.log(`il est : ${hours}`)
        return res.status(200).send(user)
      })
    } catch (err) {
      console.log(err)
    }
  })
}
