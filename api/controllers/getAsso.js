const verifyToken = require('../middleware/verifyToken.js')

module.exports = (app, db) => {
  app.get("/getAssos", verifyToken, (req, res) => {
    //http://localhost:3000/assos
    if (req.user) {
      let sql = "SELECT id, nom FROM asso WHERE statut = 1 ORDER BY id"
      db.all(sql, [], (err, result) => {
        if (err) {
          return console.error(err.message)
        }
        //console.log(result)
        res.send(result)
      })
    } else {
      console.log('token expiré !')
      return res.sendStatus(403)
    }
  })
}
