const verifyToken = require('../../middleware/verifyToken.js')

module.exports = (app, db) => {
  app.patch("/editAnimateur/:id", verifyToken, (req, res) => {
    //console.log(req)
    if (req.user) {
      let retourAjax = {}
      try {
        let sql = `UPDATE animateur SET (nom, prenom, mail, adresse, code_postal, commune, telephone) = ( TRIM(UPPER( ? )), TRIM( ? ), TRIM( ? ), TRIM( ? ), TRIM( ? ), TRIM(UPPER( ? )), TRIM( ? ) ) WHERE id = ? ;`
        let params = [req.body.nom, req.body.prenom, req.body.mail, req.body.adresse, req.body.codePostal, req.body.commune, req.body.telephone, req.body.id ]
        let stmt = db.prepare(sql);
        stmt.run(params, function(err) {
          if (err) {
            return console.error(err.message)
          }
        })
        retourAjax.state=1
        retourAjax.msg="Enregistrement effectué ! Animateur mis à jour !"
        retourAjax.rowId=req.body.id
      } catch (e) {
        console.log(e)
        retourAjax.state=0
        retourAjax.msg="Enregistrement NON effectué ! Animateur NON mis à jour !"
        retourAjax.rowId=req.body.id
      }
      //console.log(retourAjax)
      res.send(retourAjax)
    } else {
      console.log('token expiré !')
      res.status(403).send({"msg": "Session expirée !"})
    }
  })

}
