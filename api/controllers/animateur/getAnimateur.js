const verifyToken = require('../../middleware/verifyToken.js')

module.exports = (app, db) => {
  app.get("/getAnimateurs", verifyToken, (req, res) => {
    //http://localhost:3000/animateurs?saison=2021-2022
    if (req.user) {
      let sql = `SELECT DISTINCT
      a.id, a.nom, a.prenom, a.mail, a.adresse, a.code_postal AS codePostal, a.commune, a.telephone
      --, CASE
      --  WHEN group_concat(b.nom, ', ') IS NULL THEN 'NR'
      --  ELSE group_concat(b.nom, ', ')
      --END AS activites
      , 'NR' AS activites
      FROM animateur AS a
      LEFT JOIN (
        SELECT a.id_animateur, a.id_activite, c.nom
        FROM anim_acti_lnk AS a
        LEFT JOIN activite AS c ON a.id_activite = c.id
        WHERE a.statut = 1 AND c.statut = 1
        AND a.saison = ?
      ) AS b ON a.id = b.id_animateur
      WHERE a.statut = 1
      ORDER BY a.nom ASC;`
      //console.log(sql)
      let params = [req.query.saison]
      db.all(sql, params, (err, result) => {
        if (err) {
          return console.error(err.message)
        }
        //console.log(result)
        res.send(result)
      })
    } else {
      console.log('token expiré !')
      res.status(403).send({"msg": "Session expirée !"})
    }
  })

}
