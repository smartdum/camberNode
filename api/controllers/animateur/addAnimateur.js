const verifyToken = require('../../middleware/verifyToken.js')

module.exports = (app, db) => {
  app.post("/addAnimateur", verifyToken, async (req, res) => {
    //console.log(req)
    if (req.user) {
      let retourAjax = {}

      //--- VERIF ANIMATEUR
      let verif = await new Promise((resolve, reject) => {
        let sql = `SELECT count(*) AS nb FROM animateur WHERE UPPER(nom) LIKE UPPER( ? ) AND UPPER(prenom) LIKE UPPER( ? ) AND mail LIKE ? LIMIT 1;`
        //console.log(sql)
        db.all(sql, [req.body.nom, req.body.prenom, req.body.mail], function(err, rows) {
          if (err) {
            console.log(err.message)
            retourAjax.state=0
            retourAjax.msg="Enregistrement NON effectué ! Animateur NON inscrit !"
            retourAjax.rowId=null
            reject(retourAjax)
          } else {
            //console.log(rows[0])
            resolve(rows[0])
          }
        })
      })
      //console.log(verif)

      if(verif.nb == 0) {
        //--- ENREGISTREMENT ANIMATEUR
        await new Promise((resolve, reject) => {
          let sql = `INSERT INTO animateur (nom, prenom, mail, adresse, code_postal, commune, telephone) VALUES ( TRIM(UPPER( ? )), TRIM( ? ), TRIM( ? ), TRIM( ? ), TRIM( ? ), TRIM(UPPER( ? )), TRIM( ? ) );`
          let params = [req.body.nom, req.body.prenom, req.body.mail, req.body.adresse, req.body.codePostal, req.body.commune, req.body.telephone ]
          let stmt = db.prepare(sql);
          stmt.run(params, function(err) {
            if (err) {
              console.log(err.message)
              retourAjax.state=0
              retourAjax.msg="Enregistrement NON effectué ! Animateur NON inscrit !"
              retourAjax.rowId=this.lastID
              reject(retourAjax)
            } else {
              retourAjax.state=1
              retourAjax.msg="Enregistrement effectué ! Animateur inscrit !"
              retourAjax.rowId=this.lastID
              resolve(retourAjax)
            }
          })
          stmt.finalize()
        })
        console.log(retourAjax)
        res.send(retourAjax)
      } else {
        retourAjax.state=0
        retourAjax.msg="Enregistrement non effectué ! Animateur déjà inscrit !"
        retourAjax.rowId=null
        console.log(retourAjax)
        res.send(retourAjax)
      }
    } else {
      console.log('token expiré !')
      res.status(403).send({"msg": "Session expirée !"})
    }
  })

}
