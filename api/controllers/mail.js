const verifyToken = require('../middleware/verifyToken.js')
require('dotenv').config({ path: './api/middleware/.env' })
const nodeMailer = require('nodemailer');

module.exports = (app) => {
  app.post('/sendMail', verifyToken, (req, res) => {
    console.log(req)
    if (req.user) {
      const mailTo = req.body.mailTo
      console.log(mailTo)
      const mailCC = req.body.mailCC
      console.log(mailCC)
      const sujet = req.body.sujet
      console.log(sujet)
      const textHtml = req.body.textHtml
      console.log(textHtml)

      const mailFrom = process.env.EMAIL
      const transporter = nodeMailer.createTransport({
        host: process.env.SMTPHOST,
        port: 465,
        secure: true,
        requireTLS: true,
        auth: {
          user: process.env.EMAIL,
          pass: process.env.PASSWORD
        }
      })

      const mailMessage = {
        from: mailFrom,
        to: mailTo,
        subject: sujet,
        //text: textHtml,
        html: textHtml
      }

      transporter.sendMail(mailMessage, function(err, data){
        if (err) {
          console.log(err)
          res.status(400).json({"error": err.message})
        } else {
          console.log('Email envoyé: ' + data.response)
          res.status(200).json({"msg": "Email envoyé"})
        }
      })
    } else {
      console.log('token expiré !')
      return res.sendStatus(403)
    }
  })
}
