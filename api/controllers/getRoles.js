module.exports = function(app, db) {

  app.get("/getRoles", (req, res) => {
    //http://localhost:3000/roles
    let sql = "SELECT id, nom FROM role WHERE statut = 1 ORDER BY display_order"
    db.all(sql, [], (err, result) => {
      if (err) {
        return console.error(err.message)
      }
      //console.log(result)
      res.send(result)
    })
  })

}
