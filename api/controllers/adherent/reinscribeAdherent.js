const verifyToken = require('../../middleware/verifyToken.js')

module.exports = (app, db) => {
  app.post("/addAdherent", verifyToken, async (req, res) => {
    //console.log(req)
    if (req.user) {
      let retourAjax = {}

      //---- VERIF PERSONNE ENREGISTREE
      let verif = await new Promise((resolve, reject) => {
        let sql = `SELECT count(DISTINCT a.id_adherent) AS nb
        FROM adher_acti_lnk AS a
        JOIN adherent AS b ON a.id_adherent = b.id
        WHERE TRIM(UPPER(b.nom)) LIKE TRIM(UPPER( ? ))
        AND TRIM(UPPER(b.prenom)) LIKE TRIM(UPPER( ? ))
        AND TRIM(LOWER(b.mail)) LIKE TRIM(LOWER( ? ))
        AND TRIM(a.saison) = TRIM( ? );`
        let params = [req.body.nom, req.body.prenom, req.body.mail, req.body.saison]
        let stmt = db.prepare(sql);
        stmt.get(params , function(err, row) {
          if (err) {
            console.log(err)
            reject(err)
          } else {
            resolve(row)
          }
        })
        stmt.finalize()
      })
      //console.log(verif)

      if(verif.nb == 0) {
        try {
          //--- ENREGISTREMENT ADHERENT
          let insert1 = await new Promise((resolve, reject) => {
            let sql = `INSERT INTO adherent (nom, prenom, mail, nom_tuteur, prenom_tuteur, adresse, code_postal, commune, created_at)
            VALUES
            ( TRIM(UPPER( ? )), TRIM( ? ), TRIM( ? ), TRIM(UPPER( ? )), TRIM( ? ), TRIM( ? ), TRIM( ? ), TRIM(UPPER( ? )), ? );`
            let params = [req.body.nom, req.body.prenom, req.body.mail, req.body.nomTuteur, req.body.prenomTuteur, req.body.adresse, req.body.codePostal, req.body.commune, Date('now')]
            let stmt = db.prepare(sql);
            stmt.run(params, function(err) {
              if (err) {
                console.log(err)
                reject(err)
              } else {
                resolve(stmt)
              }
            })
            stmt.finalize()
          })

          //--- ENREGISTREMENT LIAISON ACTIVITE / ADHERENT
          await new Promise((resolve, reject) => {
            let sql = "INSERT INTO adher_acti_lnk (id_adherent, id_activite, saison, created_at) VALUES ( TRIM( ? ), TRIM( ? ), TRIM( ? ), ? )"
            let stmt = db.prepare(sql)
            for (var i = 0; i < req.body.activites.length; i++) {
              console.log(req.body.activites[i])
              let params = [insert1.lastID, req.body.activites[i], req.body.saison, Date('now')]
              stmt.run(params, function(err) {
                if (err) {
                  console.log(err)
                  reject(err)
                } else {
                  resolve(stmt)
                }
              })
            }
            stmt.finalize()
          })

          retourAjax.state=1
          retourAjax.msg="Enregistrement effectué ! Adhérent inscrit !"
          retourAjax.rowId=insert1.lastID
        } catch (e) {
          console.log(e)
          retourAjax.state=0
          retourAjax.msg="Enregistrement NON effectué ! Adhérent NON inscrit !"
          retourAjax.rowId=null
        }
      } else {
        retourAjax.state=0
        retourAjax.msg="Enregistrement NON effectué ! Adhérent DÉJÀ enregistré !"
        retourAjax.rowId=null
      }
      console.log(retourAjax.msg)
      res.send(retourAjax)
    } else {
      console.log('token expiré !')
      res.status(403).send({"msg": "Session expirée !"})
    }
  })

}
