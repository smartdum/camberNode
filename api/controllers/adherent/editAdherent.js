const verifyToken = require('../../middleware/verifyToken.js')

//********** A ADAPTER !!! **************//
module.exports = (app, db) => {
  app.patch("/editAdherent", verifyToken, async (req, res) => {
    //console.log(req)
    if (req.user) {
      let retourAjax = {}

      //---- VERIF PERSONNE ENREGISTREE
      let verif = await new Promise((resolve, reject) => {
        let sql = `SELECT count(DISTINCT a.id_adherent) AS nb
        FROM adher_acti_lnk AS a
        JOIN adherent AS b ON a.id_adherent = b.id
        WHERE TRIM(UPPER(b.nom)) LIKE TRIM(UPPER( ? ))
        AND TRIM(UPPER(b.prenom)) LIKE TRIM(UPPER( ? ))
        AND TRIM(LOWER(b.mail)) LIKE TRIM(LOWER( ? ))
        AND TRIM(a.saison) = TRIM( ? );`
        let params = [req.body.nom, req.body.prenom, req.body.mail, req.body.saison]
        let stmt = db.prepare(sql);
        stmt.get(params , function(err, row) {
          if (err) {
            console.log(err)
            reject(err)
          } else {
            resolve(row)
          }
        })
        stmt.finalize()
      })
      console.log(verif)

      if(verif.nb > 0) {
        try {
          //--- ENREGISTREMENT ADHERENT
          await new Promise((resolve, reject) => {
            let sql = `UPDATE adherent (nom, prenom, mail, nom_tuteur, prenom_tuteur, adresse, code_postal, commune, updated_at)
            SET
            ( TRIM(UPPER( ? )), TRIM( ? ), TRIM( ? ), TRIM(UPPER( ? )), TRIM( ? ), TRIM( ? ), TRIM( ? ), TRIM(UPPER( ? )), ? ) WHERE id = ? ;`
            console.log(sql)
            let params = [req.body.nom, req.body.prenom, req.body.mail, req.body.nomTuteur, req.body.prenomTuteur, req.body.adresse, req.body.codePostal, req.body.commune, Date('now'), req.body.id]
            let stmt = db.prepare(sql);
            stmt.run(params, function(err) {
              if (err) {
                console.log(err)
                reject(err)
              } else {
                resolve(stmt)
              }
            })
            stmt.finalize()
          })

          //--- NETTOYAGE LIAISON ACTIVITE / ADHERENT
          await new Promise((resolve, reject) => {
            console.log(req.body.activites)
            let sql = `DELETE FROM adher_acti_lnk WHERE id_adherent = ? AND id_activite = ? AND saison = ? ;`
            let stmt = db.prepare(sql)
            for (var i = 0; i < req.body.activites.length; i++) {
              console.log(req.body.activites[i])
              let params = [req.body.id, req.body.activites[i], req.body.saison, Date('now')]
              stmt.run(params, function(err) {
                if (err) {
                  console.log(err)
                  reject(err)
                } else {
                  resolve(stmt)
                }
              })
            }
            stmt.finalize()
          })

          //--- ENREGISTREMENT LIAISON ACTIVITE / ADHERENT
          await new Promise((resolve, reject) => {
            let sql = `INSERT INTO adher_acti_lnk (id_adherent, id_activite, saison, created_at) VALUES ( TRIM( ? ), TRIM( ? ), TRIM( ? ), ? );`
            let stmt = db.prepare(sql)
            for (var i = 0; i < req.body.activites.length; i++) {
              console.log(req.body.activites[i])
              let params = [req.body.id, req.body.activites[i], req.body.saison, Date('now')]
              stmt.run(params, function(err) {
                if (err) {
                  console.log(err)
                  reject(err)
                } else {
                  resolve(stmt)
                }
              })
            }
            stmt.finalize()
          })

          retourAjax.state=1
          retourAjax.msg="Enregistrement effectué ! Adhérent mis à jour !"
          retourAjax.rowId=req.body.id
        } catch (e) {
          console.log(e)
          retourAjax.state=0
          retourAjax.msg="Enregistrement NON effectué ! Adhérent NON mis à jour !"
          retourAjax.rowId=null
        }
      } else {
        retourAjax.state=0
        retourAjax.msg="Enregistrement non effectué ! Adhérent inexistant !"
        retourAjax.rowId=null
      }
      console.log(retourAjax.msg)
      res.send(retourAjax)
    } else {
      console.log('token expiré !')
      res.status(403).send({"msg": "Session expirée !"})
    }
  })

}
