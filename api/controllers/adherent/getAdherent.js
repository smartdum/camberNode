const verifyToken = require('../../middleware/verifyToken.js')

module.exports = (app, db) => {
  app.get("/getAdherents", verifyToken, (req, res) => {
    if (req.user) {
      let sql = `SELECT DISTINCT a.id, a.nom, a.prenom, a.mail
      ,group_concat(a.activite, ', ') AS activites , a.saison, b.nom AS role, 10 AS montant
      FROM (
        SELECT a.id, a.nom, a.prenom, a.mail
        , CASE
          WHEN b.id_activite IS NULL THEN 0
          ELSE b.id_activite
        END AS id_activite
        , CASE
          WHEN c.nom IS NULL THEN 'NR'
          ELSE c.nom
        END AS activite
        , b.saison
        FROM adherent AS a
        LEFT JOIN adher_acti_lnk AS b ON a.id=b.id_adherent
        LEFT JOIN activite AS c ON b.id_activite=c.id
        WHERE (a.statut = 1 AND b.statut = 1 AND c.statut = 1)
        AND b.saison = ?
      ) AS a
      LEFT JOIN (
        SELECT a.id_adherent, b.nom
        FROM adher_role_lnk AS a
        JOIN role AS b ON a.id_role = b.id
        WHERE ( a.statut = 1 AND b.statut = 1 )
        AND a.saison = ?
      ) AS b ON a.id=b.id_adherent
      GROUP BY a.id, a.nom, a.prenom, a.mail, b.nom;`
      let params = [req.query.saison]
      db.all(sql, params, (err, result) => {
        if (err) {
          return console.error(err.message)
        }
        res.send(result)
      })
    } else {
      console.log('token expiré !')
      res.status(403).send({"msg": "Session expirée !"})
    }
  })

}
