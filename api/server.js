// node ./api/server.js
const express = require("express")
const cors = require('cors')
//const path = require('path')
const bodyParser = require('body-parser')
const db = require('./db/db.js')
const hostname = 'localhost'
const port = 3000
const urlBase = "http://"+hostname+":"+port

// Création appli serveur Express
const app = express()
app.use(cors({urlBase}))
//app.options('*', cors())
app.use(express.json())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

//chargement router
require('./routes/index.js')(app, db)

// Démarrage du serveur
app.listen(port, hostname, () => {
	console.log("Serveur démarré : "+urlBase)
})

// deconnexion BDD
process.on('SIGINT', () => {
  db.close();
  //server.close();
});
